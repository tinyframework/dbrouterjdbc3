package org.tinygroup.dbrouterjdbc3.sqlprocessor.pagedata;

import org.tinygroup.jsqlparser.extend.ParameterFinder.ParameterEntry;
import org.tinygroup.jsqlparser.statement.select.Select;

/**
 * 分页数据处理接口
 * @author renhui
 *
 */
public interface PageDataProcess {

	/**
	 * 根据plainSelect查找匹配的分页数据处理接口
	 * @param plainSelect
	 * @return
	 */
	boolean isMatch(Select select);
	
	/**
	 * 获取分页数据信息
	 * @return
	 */
	PageData buildPageData(Object[] values,ParameterEntry entry);
}
