package org.tinygroup.dbrouterjdbc3.sqlprocessor.pagedata;

/**
 * 分页数据对象
 * @author renhui
 *
 */
public class PageData {
	private int startIndex = -1;

	private int limitIndex = -1;

	private Long limitValue;

	private Long startValue;

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getLimitIndex() {
		return limitIndex;
	}

	public void setLimitIndex(int limitIndex) {
		this.limitIndex = limitIndex;
	}

	public Long getLimitValue() {
		return limitValue;
	}

	public void setLimitValue(Long limitValue) {
		this.limitValue = limitValue;
	}

	public Long getStartValue() {
		return startValue;
	}

	public void setStartValue(Long startValue) {
		this.startValue = startValue;
	}
	
}
