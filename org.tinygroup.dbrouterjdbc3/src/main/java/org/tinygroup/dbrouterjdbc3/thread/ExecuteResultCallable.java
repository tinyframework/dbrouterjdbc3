package org.tinygroup.dbrouterjdbc3.thread;

import java.util.concurrent.Callable;

import org.tinygroup.commons.tools.Assert;
import org.tinygroup.dbrouter.context.RealStatementExecutor;

public class ExecuteResultCallable<T> implements Callable<T> {
	
	private StatementProcessorCallBack<T> callBack;

	private RealStatementExecutor statement;
	
	private T result;
	
	public ExecuteResultCallable(RealStatementExecutor realStatementExecutor){
		this.statement=realStatementExecutor;
	}

	public T call() throws Exception {
		Assert.assertNotNull(callBack, "callback must not null");
		if(callBack!=null){
			result= callBack.callBack(statement);
		}
		return result;
	}
	
	public T getResult(){
		return result;
	}

	public void setCallBack(StatementProcessorCallBack<T> callBack) {
		this.callBack = callBack;
	}

}
