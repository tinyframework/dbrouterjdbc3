package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.tinygroup.logger.LogLevel;
import org.tinygroup.threadgroup.AbstractProcessor;
import org.tinygroup.threadgroup.MultiThreadProcessor;

/**
 * 多客户端多线程测数据库性能测试 <br/>
 * 不同库同表
 * 
 * @author yancheng11334
 * 
 */
public class TpsExecuteTest extends BaseTpsTest {

	private static int INSERT_THREAD_NUM = 10;
	private static int INSERT_SQL_NUM = 10000;
	
	private static long PRINT_SLEEP_TIME = 10000;
	
	private static final AtomicInteger insertCount = new AtomicInteger(0);
	private static long startId = 0;
	
	private static volatile long startTime = System.currentTimeMillis();
	private static volatile long  endTime = 0;
	private static boolean autoCommit = false;
	private static boolean insertByStatement = false;
	
	/**
	 * 测试一定时间内的插入SQL性能<br/>
	 * 随机生成主键ID，可以多个客户端运行
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if(args!=null && args.length>0){
		   if(args.length>=1){
			  INSERT_THREAD_NUM = Integer.parseInt(args[0]);
		   }
		   if(args.length>=2){
			  INSERT_SQL_NUM = Integer.parseInt(args[1]);
		   }
		   if(args.length>=3){
			  insertByStatement = Boolean.parseBoolean(args[2]);
		   }
		   if(args.length>=4){
			  autoCommit = Boolean.parseBoolean(args[3]);
		   }
		}
		logger.logMessage(LogLevel.INFO, "客户端[{}]运行参数:插入线程个数:{},计划完成SQL次数:{},插入方式:{},是否自动提交:{}",System.getProperty("user.name"),INSERT_THREAD_NUM,INSERT_SQL_NUM,insertByStatement?"Statement":"PreparedStatement",autoCommit?"是":"否");
		init();
		MultiThreadProcessor processors = new MultiThreadProcessor("dbrouter");
		for (int i = 0; i < INSERT_THREAD_NUM; i++) {
			processors.addProcessor(new InsertThread("insert-" + i));
		}
		processors.addProcessor(new PrintThread("print"));

		startTime = System.currentTimeMillis();
		processors.start();
		
		long costtime = endTime - startTime;
		logger.logMessage(LogLevel.INFO, "客户端[{}]运行参数:插入线程个数:{},计划完成SQL次数:{},插入方式:{},是否自动提交:{}",System.getProperty("user.name"),INSERT_THREAD_NUM,INSERT_SQL_NUM,insertByStatement?"Statement":"PreparedStatement",autoCommit?"是":"否");
		logger.logMessage(LogLevel.INFO, "客户端[{}]结果分析:完成SQL次数:{},插入线程个数:{},插入总耗时:{}ms,平均耗时:{}ms,本客户端每秒完成插入次数:{}",System.getProperty("user.name"),
				INSERT_SQL_NUM, INSERT_THREAD_NUM, costtime, costtime
				/INSERT_SQL_NUM,INSERT_SQL_NUM*1000D/costtime);	
	}
	
	/**
	 * 随机方式产生记录Id,多客户端运行冲突的概率约万分之几
	 * @return
	 */
	protected static long getStartId(){
		Random r= new Random();
		long temp = Math.abs(r.nextLong())%(INSERT_SQL_NUM>10000?INSERT_SQL_NUM:10000);
		return temp*INSERT_SQL_NUM;
	}
	
	private static void init() throws Exception{
		startId = getStartId();
		 Connection conn = null;
		 try{
			 conn = getConnection();
		 }finally{
			 close(conn);
		 } 
	}
	
	protected static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	
	/**
	 * 输出线程(中间输出仅供估计，最终结果以主线程统计值为准)
	 * @author yancheng11334
	 *
	 */
	static class PrintThread extends AbstractProcessor {

		public PrintThread(String name) {
			super(name);
		}

		@Override
		protected void action() throws Exception {
			while (insertCount.get() < INSERT_SQL_NUM) {
				long costtime = System.currentTimeMillis() - startTime;
				logger.logMessage(LogLevel.INFO, "客户端插入进度:完成SQL次数:{},插入线程个数:{},插入总耗时:{}ms,平均耗时:{}ms,本客户端每秒完成插入次数:{}",
						insertCount.get(), INSERT_THREAD_NUM, costtime, costtime
						/ (insertCount.get()==0?1:insertCount.get()),insertCount.get()*1000D/costtime);	
				
				Thread.sleep(PRINT_SLEEP_TIME);
			}
		}
		
	}
	
	/**
	 * 插入线程
	 * @author yancheng11334
	 *
	 */
	static class InsertThread extends AbstractProcessor {
		private int count = 0;
		public InsertThread(String name) {
			super(name);
		}

		@Override
		protected void action() throws Exception {
			logger.logMessage(LogLevel.INFO, "{}线程开始执行插入SQL...", getName());
			if(insertByStatement){
				insertByStatement();
			}else{
				insertByPreparedStatement();
			}
			if(endTime==0){
			   endTime = System.currentTimeMillis();
			}
			logger.logMessage(LogLevel.INFO, "{}线程插入完毕,完成SQL次数:{}", getName(),
					count);
		}
		
		private void insertByPreparedStatement() throws Exception {
			Connection conn = null;
			PreparedStatement pStmt = null;
			try {
				//测试长连接(1个线程生命周期只创建一次连接)
				conn = getConnection();
				conn.setAutoCommit(false);
				pStmt = conn.prepareStatement("insert into score(id,name,score,course) values(?,?,?,?)");
				pStmt.setQueryTimeout(0);
				int r=0;
				while ((r=insertCount.getAndIncrement()) < INSERT_SQL_NUM) {
					long id = startId+r;
					count++;
					pStmt.setLong(1, id);
					pStmt.setString(2, "yc"+count);
					pStmt.setInt(3, 60);
					pStmt.setString(4, "english");
					pStmt.addBatch();
					if(count%1000==0){
						pStmt.executeBatch();
					}
				}
				pStmt.executeBatch();
				conn.commit();
			} finally {
				close(conn);
				close(pStmt);
			}
		}
		
		private void insertByStatement() throws Exception {
			Connection conn = null;
			Statement st = null;
			try {
				//测试长连接(1个线程生命周期只创建一次连接)
				conn = getConnection();
				conn.setAutoCommit(autoCommit);
				st = conn.createStatement();
				st.setQueryTimeout(0);
				int r=0;
				while ((r=insertCount.getAndIncrement()) < INSERT_SQL_NUM) {
					long id = startId+r;
					count++;
					st.executeUpdate("insert into score (id,name,score,course) values("+id+",'yc'+count,60,'english')");
				}
				conn.commit();
				
				//测试短连接(1个线程每次抢到时间段，创建一个连接)
//				int r=0;
//				while ((r=insertCount.getAndIncrement()) < INSERT_SQL_NUM) {
//					long id = startId+r;
//					count++;
//					conn = getConnection();
//					st = conn.createStatement();
//					st.executeUpdate("insert into score (id,name,score,course) values("+id+",'yancheng',60,'english')");
//					close(conn);
//					close(st);
//				}
			} finally {
				close(conn);
				close(st);
			}
		}
		
	}
	
}
