package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.enhydra.jdbc.standard.StandardXADataSource;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;

/**
 * 初始化测试数据工具类
 * 
 * @author yancheng11334
 * 
 */
public class InitPerformanceDataTest {

	private static Logger logger = LoggerFactory
			.getLogger(InitPerformanceDataTest.class);

	//public static String DB_PATH= "localhost";
	public static String DB_PATH= "mysqldb";
	public static int CREATE_RECORD_NUM = 5000000;

	public static void main(String[] args) throws Exception {
		InitPerformanceDataTest test = new InitPerformanceDataTest();
		//test.initSameDataSource();
		test.initDiffDataSource();
		//test.initDiffTableDataSource();
	}

	/**
	 * 初始化单库数据(记录数=CREATE_RECORD_NUM)
	 * 
	 * @throws Exception
	 */
	public void initSameDataSource() throws Exception {
		StandardXADataSource ds = initDataSource(
				"jdbc:mysql://"+DB_PATH+":3306/test", "root", "123456");
		try {
			// 先查询数据库记录，如果记录数比定义的少，删除原有记录并重新创建。
			int count = getSchemaCountNum(ds);
			if (count < CREATE_RECORD_NUM) {
				logger.logMessage(LogLevel.INFO,
						"实际记录数:{},查询记录最少记录数{},需重新初始化数据", count,
						CREATE_RECORD_NUM);
				if (count > 0) {
					clearRecordForSameSchema(ds);
				}
				createRecordForSameSchema(ds);
			} else {
				logger.logMessage(LogLevel.INFO, "实际记录数:{},无需重新初始化数据", count);
			}
		} finally {
			if (ds != null) {
				ds.closeFreeConnection();
			}
		}
	}

	/**
	 * 初始化不同库同表数据(所有库记录数之和=CREATE_RECORD_NUM)
	 * 
	 * @throws Exception
	 */
	public void initDiffDataSource() throws Exception {

		// 先查询数据库记录，如果记录数比定义的少，删除原有记录并重新创建。
		String[] urls = { "jdbc:mysql://192.168.50.201:3306/test",
				"jdbc:mysql://192.168.50.202:3306/test",
				"jdbc:mysql://192.168.50.203:3306/test",
				"jdbc:mysql://192.168.50.204:3306/test",
				"jdbc:mysql://192.168.50.205:3306/test"};
		String user = "root";
		String password = "123456";
		StandardXADataSource ds = null;
		for (int i = 0; i < urls.length; i++) {
			try {
				ds = initDataSource(urls[i], user, password);
				int count = getSchemaCountNum(ds);
				if (count < CREATE_RECORD_NUM / urls.length) {
					logger.logMessage(LogLevel.INFO,
							"实际记录数:{},查询记录最少记录数{},需重新初始化数据", count,
							CREATE_RECORD_NUM / urls.length);
					if (count > 0) {
						clearRecordForDiffSchema(ds);
					}
					createRecordForDiffSchema(ds, urls.length, i);
				} else {
					logger.logMessage(LogLevel.INFO, "实际记录数:{},无需重新初始化数据",
							count);
				}
			} finally {
				if (ds != null) {
					ds.closeFreeConnection();
				}
			}
		}
	}

	/**
	 * 初始化同库不同表数据(所有库记录数之和=CREATE_RECORD_NUM)
	 * 
	 * @throws Exception
	 */
	public void initDiffTableDataSource() throws Exception {

		StandardXADataSource ds = initDataSource(
				"jdbc:mysql://"+DB_PATH+":3306/sametest", "root", "123456");
		String[] tables = {"score11","score12","score13","score14","score15","score16","score17","score18","score19","score10"};
		try {
			// 先查询数据库记录，如果记录数比定义的少，删除原有记录并重新创建。
			int count = getDiffTableCountNum(ds,tables);
			if (count < CREATE_RECORD_NUM) {
				logger.logMessage(LogLevel.INFO,
						"实际记录数:{},查询记录最少记录数{},需重新初始化数据", count,
						CREATE_RECORD_NUM);
				if (count > 0) {
					clearRecordForDiffTable(ds,tables);
				}
				createRecordForDiffTable(ds,tables);
			} else {
				logger.logMessage(LogLevel.INFO, "实际记录数:{},无需重新初始化数据", count);
			}
		} finally {
			if (ds != null) {
				ds.closeFreeConnection();
			}
		}
	}

	private StandardXADataSource initDataSource(String url, String user,
			String pass) throws Exception {
		StandardXADataSource dataSource = new StandardXADataSource();
		dataSource.setUrl(url);
		dataSource.setDriverName("com.mysql.jdbc.Driver");
		dataSource.setUser(user);
		dataSource.setPassword(pass);
		return dataSource;
	}

	/**
	 * 产生单库查询数据
	 */
	private void createRecordForSameSchema(StandardXADataSource dataSource)
			throws Exception {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			logger.logMessage(LogLevel.INFO, "开始产生单库查询数据...");
			conn = dataSource.getXAConnection().getConnection();
			pStmt = conn
					.prepareStatement("insert into score1(id,name,score,course) values(?,?,?,?)");
			conn.setAutoCommit(false);
			int BATCH_MAX_SIZE = 200;
			int k = 0;
			int create_num = 0;
			for (int i = 1; i <= CREATE_RECORD_NUM; i++) {
				k++;
				pStmt.setLong(1, i);
				pStmt.setString(2, "yancheng");
				pStmt.setInt(3, 60);
				pStmt.setString(4, "shuxue");
				pStmt.addBatch();
				if (k == BATCH_MAX_SIZE) {
					create_num += BATCH_MAX_SIZE;
					logger.logMessage(LogLevel.INFO, "已经产生{}条查询数据...",
							create_num);
					pStmt.executeBatch();
					k = 0;
				}
			}
			if (k > 0) {
				pStmt.executeBatch();
			}
			conn.commit();
			conn.setAutoCommit(true);
			logger.logMessage(LogLevel.INFO, "产生单库查询数据结束,计划产生记录数:{}",
					CREATE_RECORD_NUM);
		} finally {
			close(conn);
			close(pStmt);
		}
	}

	/**
	 * 清除单库查询数据
	 */
	private void clearRecordForSameSchema(StandardXADataSource dataSource)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			logger.logMessage(LogLevel.INFO, "开始清除单库查询数据...");
			conn = dataSource.getXAConnection().getConnection();
			stmt = conn.createStatement();
			stmt.execute("delete from score1");
		} finally {
			close(conn);
			close(stmt);
		}
		logger.logMessage(LogLevel.INFO, "清除单库查询数据结束!");
	}

	/**
	 * 清除多库查询数据
	 */
	private void clearRecordForDiffSchema(StandardXADataSource dataSource)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			logger.logMessage(LogLevel.INFO, "开始清除库{}的查询数据...",
					dataSource.getUrl());
			conn = dataSource.getXAConnection().getConnection();
			stmt = conn.createStatement();
			stmt.execute("delete from score1");
		} finally {
			close(conn);
			close(stmt);
		}
		logger.logMessage(LogLevel.INFO, "清除库{}的查询数据结束!", dataSource.getUrl());
	}
	
	/**
	 * 清除同库不同表查询数据
	 */
	private void clearRecordForDiffTable(StandardXADataSource dataSource,String[] tables)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			logger.logMessage(LogLevel.INFO, "开始清除同库不同表查询数据...");
			conn = dataSource.getXAConnection().getConnection();
			stmt = conn.createStatement();
			for(int i=0;i<tables.length;i++){
				stmt.execute("delete from "+tables[i]);
			}
			
		} finally {
			close(conn);
			close(stmt);
		}
		logger.logMessage(LogLevel.INFO, "清除同库不同表查询数据结束!");
	}

	/**
	 * 产生多库查询数据
	 */
	private void createRecordForDiffSchema(StandardXADataSource dataSource,
			int totalnum, int remainder) throws Exception {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			logger.logMessage(LogLevel.INFO, "开始产生库{}的查询数据...",
					dataSource.getUrl());
			conn = dataSource.getXAConnection().getConnection();
			pStmt = conn
					.prepareStatement("insert into score1(id,name,score,course) values(?,?,?,?)");
			conn.setAutoCommit(false);
			int BATCH_MAX_SIZE = 200;
			int k = 0;
			int create_num = 0;
			for (int i = 1 + remainder; i <= CREATE_RECORD_NUM; i = i
					+ totalnum) {
				k++;
				pStmt.setLong(1, i);
				pStmt.setString(2, "yancheng");
				pStmt.setInt(3, 60);
				pStmt.setString(4, "shuxue");
				pStmt.addBatch();
				if (k == BATCH_MAX_SIZE) {
					create_num += BATCH_MAX_SIZE;
					logger.logMessage(LogLevel.INFO, "已经产生{}条查询数据...",
							create_num);
					pStmt.executeBatch();
					k = 0;
				}
			}
			if (k > 0) {
				pStmt.executeBatch();
			}
			conn.commit();
			conn.setAutoCommit(true);
			logger.logMessage(LogLevel.INFO, "产生库{}的查询数据结束,计划产生记录数:{}",
					dataSource.getUrl(), CREATE_RECORD_NUM / totalnum);
		} finally {
			close(conn);
			close(pStmt);
		}
	}
	
	/**
	 * 产生同库不同表查询数据
	 */
	private void createRecordForDiffTable(StandardXADataSource dataSource,
			String[] tables) throws Exception {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			conn = dataSource.getXAConnection().getConnection();
			
			for(int jj=0;jj<tables.length;jj++){
				logger.logMessage(LogLevel.INFO, "开始产生表{}的查询数据...",
						tables[jj]);
				
				pStmt = conn
						.prepareStatement("insert into "+tables[jj]+" (id,name,score,course) values(?,?,?,?)");
				conn.setAutoCommit(false);
				int BATCH_MAX_SIZE = 200;
				int k = 0;
				int create_num = 0;
				for (int i = 1 + jj; i <= CREATE_RECORD_NUM; i = i
						+ tables.length) {
					k++;
					pStmt.setLong(1, i);
					pStmt.setString(2, "yancheng");
					pStmt.setInt(3, 60);
					pStmt.setString(4, "shuxue");
					pStmt.addBatch();
					if (k == BATCH_MAX_SIZE) {
						create_num += BATCH_MAX_SIZE;
						logger.logMessage(LogLevel.INFO, "已经产生{}条查询数据...",
								create_num);
						pStmt.executeBatch();
						k = 0;
					}
				}
				if (k > 0) {
					pStmt.executeBatch();
				}
				conn.commit();
				conn.setAutoCommit(true);
				logger.logMessage(LogLevel.INFO, "产生表{}的查询数据结束,计划产生记录数:{}",
						tables[jj], CREATE_RECORD_NUM / tables.length);
			}
			
		} finally {
			close(conn);
			close(pStmt);
		}
	}

	private int getSchemaCountNum(StandardXADataSource dataSource)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getXAConnection().getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select count(1) as total from score1");
			rs.next();
			return rs.getInt("total");
		} finally {
			close(conn);
			close(stmt);
			close(rs);
		}
	}

	private int getDiffTableCountNum(StandardXADataSource dataSource,
			String[] tablenames) throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int num = 0;
		try {
			conn = dataSource.getXAConnection().getConnection();
			stmt = conn.createStatement();
			for (int i = 0; i < tablenames.length; i++) {
				rs = stmt.executeQuery("select count(1) as total from "
						+ tablenames[i]);
				rs.next();
				num += rs.getInt("total");
			}

			return num;
		} finally {
			close(conn);
			close(stmt);
			close(rs);
		}
	}

	protected void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(PreparedStatement pStmt) {
		if (pStmt != null) {
			try {
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
