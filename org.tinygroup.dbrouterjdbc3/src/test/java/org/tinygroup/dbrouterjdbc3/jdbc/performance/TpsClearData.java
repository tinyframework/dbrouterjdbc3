package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.tinygroup.logger.LogLevel;

/**
 * 清除TPS的测试数据
 * 
 * @author yancheng11334
 * 
 */
public class TpsClearData extends BaseTpsTest {

	private static String sql = "select count(1) from score";

	/**
	 * 清理多线程IO测试产生的测试数据
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TpsClearData job = new TpsClearData();
		logger.logMessage(LogLevel.INFO, "清理前数据库的测试记录数:{}", job.getCountNum());
		job.clearData();
		logger.logMessage(LogLevel.INFO, "清理后数据库的测试记录数:{}", job.getCountNum());
	}
	
	protected static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	
	public int getCountNum() throws Exception{
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			st = conn.createStatement();
			st.setQueryTimeout(0); 
			rs = st.executeQuery(sql);
			rs.next();
			return rs.getInt(1);
		} finally {
			close(conn);
			close(st);
			close(rs);
		}
	}
	
	public void clearData() throws Exception{
		Connection conn = null;
		Statement st = null;
		try {
			conn = getConnection();
			st = conn.createStatement();
			st.setQueryTimeout(0); //时间可以设置长些，避免超时导致Query execution was interrupted 
			st.executeUpdate("delete from score");
		} finally {
			close(conn);
			close(st);
		}
	}

}
