package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.enhydra.jdbc.standard.StandardXADataSource;
import org.tinygroup.dbrouter.RouterManager;
import org.tinygroup.dbrouter.factory.RouterManagerBeanFactory;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;

/**
 * 大数据单线程性能对比测试
 * @author yancheng11334
 *
 */
public class DataPerformanceTest{
	
	private static Logger logger = LoggerFactory.getLogger(DataPerformanceTest.class);
	
//	private static final String ROUTER_XML ="selectDiffSchema.xml";
	//private static final String ROUTER_XML ="selectSameSchema.xml";
	private static final String ROUTER_XML ="selectDiffMachine.xml";
	private static final String ROUTER_ID ="selectTest";
	private static final String USER ="luog";
	private static final String PASSWORD ="123456";
	
	private static final String ROUTER_XML2 ="selectDiffTable.xml";
	private static final String ROUTER_ID2 ="diffTable";
	
	StandardXADataSource dataSource=null;
	
	private Connection commonConnection;
	
	private Connection diffSchemaConnection;
	
	private Connection diffTableConnection;
	
	
	public static void main(String[] args) throws Exception{
		DataPerformanceTest test = new DataPerformanceTest();
		test.init();
		test.compareBigDataSelectPerformance();
		test.compareBigDataUpdatePerformance();
		//test.compareBigDataInsertPerformance();
		//test.compareBigDataDeletePerformance();
		test.close();
	}
	
	private  void close() {
		close(commonConnection);
		close(diffSchemaConnection);
		close(diffTableConnection);
	}

	public void init() throws Exception{
		dataSource = new StandardXADataSource();
    	dataSource.setUrl("jdbc:mysql://mysqldb:3306/test");
		dataSource.setDriverName("com.mysql.jdbc.Driver");
		dataSource.setUser("root");
		dataSource.setPassword("123456");
		
		RouterManager routerManager = RouterManagerBeanFactory.getManager();
		routerManager.addRouters("/"+ROUTER_XML);
		routerManager.addRouters("/"+ROUTER_XML2);
		Class.forName("org.tinygroup.dbrouterjdbc3.jdbc.TinyDriver");
		commonConnection=dataSource.getXAConnection().getConnection();
		diffSchemaConnection=DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID, USER,PASSWORD);
		diffTableConnection=DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID2, USER,PASSWORD);
	}
	
	/**
	 * 测试数据库大数据量下的查询性能
	 * @throws Exception
	 */
	public void compareBigDataSelectPerformance() throws Exception{
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的查询性能对比开始...");
		int count = getSameSchemaCountNum();
		long time = System.currentTimeMillis();
		bigDataCommonSelect(commonConnection);
		logger.logMessage(LogLevel.INFO, "总记录数:{},单库查询耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentSchemaCountNum();
		time = System.currentTimeMillis();
		bigDataCommonSelect(diffSchemaConnection);
		logger.logMessage(LogLevel.INFO, "总记录数:{},不同库同表查询耗时:{}ms",count,System.currentTimeMillis()-time);
		
	    count = getDifferentTableCountNum();
		time = System.currentTimeMillis();
		bigDataCommonSelect(diffTableConnection);
		logger.logMessage(LogLevel.INFO, "总记录数:{},同库不同表查询耗时:{}ms",count,System.currentTimeMillis()-time);
		
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的查询性能对比结束!");
	}
	
	/**
	 * 测试数据库大数据量下的新增性能
	 * @throws Exception
	 */
	public void compareBigDataInsertPerformance() throws Exception{
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的新增性能对比开始...");
    	int count = getSameSchemaCountNum();
		long time = System.currentTimeMillis();
		bigDataCommonInsert(dataSource.getXAConnection().getConnection());
		logger.logMessage(LogLevel.INFO, "总记录数:{},单库新增耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentSchemaCountNum();
		time = System.currentTimeMillis();
		bigDataCommonInsert(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},不同库同表新增耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentTableCountNum();
		time = System.currentTimeMillis();
		bigDataCommonInsert(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID2, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},同库不同表新增耗时:{}ms",count,System.currentTimeMillis()-time);
		
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的新增性能对比结束!");
	}
	
	/**
	 * 测试数据库大数据量下的更新性能
	 * @throws Exception
	 */
    public void compareBigDataUpdatePerformance() throws Exception{
    	logger.logMessage(LogLevel.INFO,"数据库大数据量下的更新性能对比开始...");
    	int count = getSameSchemaCountNum();
		long time = System.currentTimeMillis();
		bigDataCommonUpdate(dataSource.getXAConnection().getConnection());
		logger.logMessage(LogLevel.INFO, "总记录数:{},单库更新耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentSchemaCountNum();
		time = System.currentTimeMillis();
		bigDataCommonUpdate(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},不同库同表更新耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentTableCountNum();
		time = System.currentTimeMillis();
		bigDataCommonUpdate(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID2, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},同库不同表更新耗时:{}ms",count,System.currentTimeMillis()-time);
		
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的更新性能对比结束!");
	}
    
    /**
	 * 测试数据库大数据量下的删除性能
	 * @throws Exception
	 */
    public void compareBigDataDeletePerformance() throws Exception{
    	logger.logMessage(LogLevel.INFO,"数据库大数据量下的删除性能对比开始...");
    	int count = getSameSchemaCountNum();
		long time = System.currentTimeMillis();
		bigDataCommonDelete(dataSource.getXAConnection().getConnection());
		logger.logMessage(LogLevel.INFO, "总记录数:{},单库删除耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentSchemaCountNum();
		time = System.currentTimeMillis();
		bigDataCommonDelete(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},不同库同表删除耗时:{}ms",count,System.currentTimeMillis()-time);
		
		count = getDifferentTableCountNum();
		time = System.currentTimeMillis();
		bigDataCommonDelete(DriverManager.getConnection("jdbc:dbrouter://"+ROUTER_ID2, USER,PASSWORD));
		logger.logMessage(LogLevel.INFO, "总记录数:{},同库不同表删除耗时:{}ms",count,System.currentTimeMillis()-time);
		
		logger.logMessage(LogLevel.INFO,"数据库大数据量下的删除性能对比结束!");
	}
	
    /**
     * 公用插入语句
     * @param conn
     * @throws Exception
     */
    private void bigDataCommonInsert(Connection conn) throws Exception{
		PreparedStatement pStmt = null;
		String sql = null;
		try{
			sql = "insert into score1 (id,name,score,course) values(?,?,?,?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setLong(1, 7500006L);
			pStmt.setString(2, "cat");
			pStmt.setInt(3, 60);
			pStmt.setString(4, "yuwen");
			logger.logMessage(LogLevel.INFO, "执行SQL:{}", sql);
			int num = pStmt.executeUpdate();
			logger.logMessage(LogLevel.INFO, "插入记录数目:{}",num);
			
		}finally{
			close(conn);
	    	close(pStmt);
		}		
    }
    
    /**
     * 公用删除语句
     * @param conn
     * @throws Exception
     */
    private void bigDataCommonDelete(Connection conn) throws Exception{
		PreparedStatement pStmt = null;
		String sql = null;
		try{
		    sql = "delete from score1 where id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setLong(1, 7500006L);
			logger.logMessage(LogLevel.INFO, "执行SQL:{}", sql);
			int num = pStmt.executeUpdate();
			logger.logMessage(LogLevel.INFO, "删除记录数目:{}",num);
			
		}finally{
			close(conn);
	    	close(pStmt);
		}		
    }
    
    /**
     * 公用更新语句
     * @param conn
     * @throws Exception
     */
    private void bigDataCommonUpdate(Connection conn) throws Exception{
		PreparedStatement pStmt = null;
		String sql = null;
		int num =0;
		try{
//		    sql = "update score1 set name= ? ,score = ? where id = ? ";
//			pStmt = conn.prepareStatement(sql);
//			pStmt.setString(1, "pili1");
//			pStmt.setInt(2, 40);
//			pStmt.setLong(3, 3000007L);
//			logger.logMessage(LogLevel.INFO, "执行SQL:{}", sql);
//			num = pStmt.executeUpdate();
//			logger.logMessage(LogLevel.INFO, "更新记录数目:{}",num);
			
			sql = "update score1 set name= ? where name = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "yancheng");
			pStmt.setString(2, "pili1");
			logger.logMessage(LogLevel.INFO, "执行SQL:{}", sql);
			num = pStmt.executeUpdate();
			logger.logMessage(LogLevel.INFO, "更新记录数目:{}",num);
		}finally{
			close(conn);
	    	close(pStmt);
		}		
    }
    
    /**
     * 公用查询语句
     * @param conn
     * @throws Exception
     */
    private void bigDataCommonSelect(Connection conn) throws Exception{
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String sql = null;
		try{
			//索引
//			sql = "select * from score1 where id in (?,?,?)";
//			pStmt = conn.prepareStatement(sql);
//			pStmt.setLong(1, 4900000L);
//			pStmt.setLong(2, 150000L);
//			pStmt.setLong(3, 11L);
			
			//非索引
			sql = "select * from score1 where name = ? ";
			pStmt = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			pStmt.setString(1, "dog");
			logger.logMessage(LogLevel.INFO, "执行SQL:{}", sql);
			rs = pStmt.executeQuery();
			
//			while(rs.next()){
//			   logger.logMessage(LogLevel.INFO, "找到记录id:{},name:{},score:{},course:{}",rs.getString("id"),rs.getString("name"),rs.getInt("score"),rs.getString("course"));
//			}
		}finally{
	    	close(pStmt);
	    	close(rs);
		}		
    }
    
    private int getSameSchemaCountNum() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		try{
		    stmt = commonConnection.createStatement();
		    rs =stmt.executeQuery("select count(1) as total from score1");
		    rs.next();
		    return rs.getInt("total");
		}finally{
	    	close(stmt);
	    	close(rs);
		}
    }
    
    private int getDifferentSchemaCountNum() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		try{
		    stmt = diffSchemaConnection.createStatement();
		    rs =stmt.executeQuery("select count(1) as total from score1");
		    rs.next();
		    return rs.getInt("total");
		}finally{
	    	close(stmt);
	    	close(rs);
		}
    }
    
    private int getDifferentTableCountNum() throws Exception {
    	Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
		    stmt = diffTableConnection.createStatement();
		    rs =stmt.executeQuery("select count(1) as total from score1");
		    rs.next();
		    return rs.getInt("total");
		}finally{
			close(conn);
	    	close(stmt);
	    	close(rs);
		}
    }
	
	
	protected void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(PreparedStatement pStmt) {
		if (pStmt != null) {
			try {
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
