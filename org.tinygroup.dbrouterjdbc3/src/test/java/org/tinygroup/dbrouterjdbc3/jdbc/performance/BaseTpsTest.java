package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.tinygroup.dbrouter.RouterManager;
import org.tinygroup.dbrouter.factory.RouterManagerBeanFactory;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;

public class BaseTpsTest {
    static Logger logger = LoggerFactory.getLogger("TpsTest");
	
    protected static RouterManager routerManager;
	protected static String driver = "org.tinygroup.dbrouterjdbc3.jdbc.TinyDriver";
	protected static String user = "luog";
	protected static String password = "123456";

	// 同库不同表(不同设备)
	protected static String routerpath = "/scoreDiffMachinePerformance.xml";
	protected static String url = "jdbc:dbrouter://scoreDiffMachinePerformance";
	
	static {
		routerManager = RouterManagerBeanFactory.getManager();
		routerManager.addRouters(routerpath);
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	protected static void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected static void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected static void close(PreparedStatement pStmt) {
		if (pStmt != null) {
			try {
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
