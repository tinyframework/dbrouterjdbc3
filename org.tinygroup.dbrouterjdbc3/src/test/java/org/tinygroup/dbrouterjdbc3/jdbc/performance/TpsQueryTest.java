package org.tinygroup.dbrouterjdbc3.jdbc.performance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.tinygroup.logger.LogLevel;
import org.tinygroup.threadgroup.AbstractProcessor;
import org.tinygroup.threadgroup.MultiThreadProcessor;

/**
 * 多客户端多线程测数据库性能测试 <br/>
 * 不同库同表
 * 
 * @author yancheng11334
 * 
 */
public class TpsQueryTest extends BaseTpsTest {
	
	private static final String ALL_SQL = "select count(1) from score";
	private static final String ID_SQL = "select * from score where id = ";
	private static final String NAME_SQL = "select * from score where name = ";
	private static final String UPDATE_ID_SQL = "update score set name='dog' where id = ";
	private static final String UPDATE_NAME_SQL = "update score set name='dog' where name = ";
	
	private static String QUERY_TYPE = "name";
	private static String show_name ="";
	private static String sql = NAME_SQL;
	

	private static int QUERY_THREAD_NUM = 10;
	private static int QUERY_SQL_NUM = 10000; 
	
	
	private static long PRINT_SLEEP_TIME = 10000;
	private static volatile int  resultNum = 0;
	
	static Random r= new Random();
	private static final AtomicInteger queryCount = new AtomicInteger(0);
	private static volatile long startTime = System.currentTimeMillis();
	private static volatile long  endTime = 0;

	/**
	 * 测试一定时间内的SQL性能
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		
		if(args!=null && args.length>0){
		   if(args.length>=1){
			   QUERY_THREAD_NUM = Integer.parseInt(args[0]);
		   }
		   if(args.length>=2){
			   QUERY_SQL_NUM = Integer.parseInt(args[1]);
		   }
		   if(args.length>=3){
			  if(args[2].equalsIgnoreCase("id")){
				  QUERY_TYPE = "id";
				  sql = ID_SQL;
				  show_name="查询";
			  }else if(args[2].equalsIgnoreCase("all")){
				  QUERY_TYPE = "all";
				  sql = ALL_SQL;
				  show_name="查询";
			  }else if(args[2].equalsIgnoreCase("name")){
				  QUERY_TYPE = "name";
				  sql = NAME_SQL;
				  show_name="查询";
			  }else if(args[2].equalsIgnoreCase("uid")){
				  QUERY_TYPE = "uid";
				  sql = UPDATE_ID_SQL;
				  show_name="更新";
			  }else if(args[2].equalsIgnoreCase("uname")){
				  QUERY_TYPE = "uname";
				  sql = UPDATE_NAME_SQL;
				  show_name="更新";
			  }
		   }
		}
		logger.logMessage(LogLevel.INFO, "客户端[{}]运行参数:"+show_name+"线程个数:{},计划完成SQL次数:{}",System.getProperty("user.name"),QUERY_THREAD_NUM,QUERY_SQL_NUM);
		init();
		
		MultiThreadProcessor processors = new MultiThreadProcessor("dbrouter");
		for (int i = 0; i < QUERY_THREAD_NUM; i++) {
			processors.addProcessor(new QueryThread("query-" + i));
		}
		processors.addProcessor(new PrintThread("print"));

		startTime = System.currentTimeMillis();
		processors.start();
		
		long costtime = endTime - startTime;
		logger.logMessage(LogLevel.INFO, "客户端[{}]运行参数:"+show_name+"线程个数:{},计划完成SQL次数:{}",System.getProperty("user.name"),QUERY_THREAD_NUM,QUERY_SQL_NUM);
		logger.logMessage(LogLevel.INFO,
				"客户端[{}]结果分析: 完成SQL次数:{},"+show_name+"线程个数:{},"+show_name+"总耗时:{}ms,平均耗时:{}ms,本客户端每秒完成"+show_name+"次数:{}",System.getProperty("user.name"),
				QUERY_SQL_NUM, QUERY_THREAD_NUM, costtime, costtime
						/ QUERY_SQL_NUM,QUERY_SQL_NUM*1000D/costtime);
	}
	
	protected static long getRecordId(){
		long temp = Math.abs(r.nextLong());
		return temp;
	}
	
	protected static String getRecordName(){
		String name = "yc"+getRecordId();
		return name;
	}

	protected static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
		
//		StandardXADataSource dataSource = new StandardXADataSource();
//		dataSource.setUrl("jdbc:mysql://192.168.50.201:3306/tps");
//		dataSource.setDriverName("com.mysql.jdbc.Driver");
//		dataSource.setUser("root");
//		dataSource.setPassword("123456");
//		return dataSource.getXAConnection().getConnection();
	}
	
	//执行SQL,初始化jotm
	public static void init() throws SQLException{
		 Connection conn = null;
		 try{
			 conn = getConnection();
		 }finally{
			 close(conn);
		 }
	}

	/**
	 * 输出线程(中间输出仅供估计，最终结果以主线程统计值为准)
	 * @author yancheng11334
	 *
	 */
	static class PrintThread extends AbstractProcessor {

		public PrintThread(String name) {
			super(name);
		}

		@Override
		protected void action() throws Exception {
			while (queryCount.get() < QUERY_SQL_NUM) {
				
				long costtime = System.currentTimeMillis() - startTime;
				logger.logMessage(LogLevel.INFO, "客户端"+show_name+"进度:完成SQL次数:{},"+show_name+"线程个数:{},"+show_name+"总耗时:{}ms,平均耗时:{}ms,本客户端每秒完成"+show_name+"次数:{}",
						queryCount.get(), QUERY_THREAD_NUM, costtime, costtime
						/ (queryCount.get()==0?1:queryCount.get()),queryCount.get()*1000D/costtime);	
				
				Thread.sleep(PRINT_SLEEP_TIME);
			}
			
		}

	}

	/**
	 * 操作线程
	 * @author yancheng11334
	 *
	 */
	static class QueryThread extends AbstractProcessor {

		private int count = 0;
		public QueryThread(String name) {
			super(name);
		}

		@Override
		protected void action() throws Exception {
			logger.logMessage(LogLevel.INFO, "{}线程开始执行"+show_name+"SQL:{}...", getName(),sql);
			if(QUERY_TYPE.equals("id")){
				queryById();
			}else if(QUERY_TYPE.equals("name")){
				queryByName();
			}else if(QUERY_TYPE.equals("all")){
				queryCount();
			}else if(QUERY_TYPE.equals("uid")){
				updateById();
			}else if(QUERY_TYPE.equals("uname")){
				updateByName();
			}else{
				logger.logMessage(LogLevel.INFO,"没有找到匹配的操作!");
			}
			if(endTime==0){
			   endTime = System.currentTimeMillis();
			}
			logger.logMessage(LogLevel.INFO, "{}线程"+show_name+"完毕,完成SQL次数:{}", getName(),
					count);
		}

		private void queryCount() throws Exception {
                //测试长连接(1个线程生命周期只创建一次连接)
			    Connection conn = null;
			    Statement st = null;
			    ResultSet rs = null;
				try{
					conn = getConnection();
					st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    st.setQueryTimeout(0);
					while (queryCount.getAndIncrement() < QUERY_SQL_NUM) {
						count++;
						rs = st.executeQuery(sql);
						rs.next();
						resultNum = rs.getInt(1);
					}
				}finally{
					close(conn);
					close(st);
					close(rs);
				}
		}
		
		private void queryByName() throws Exception {
            //测试长连接(1个线程生命周期只创建一次连接)
		    Connection conn = null;
		    Statement st = null;
		    ResultSet rs = null;
			try{
				conn = getConnection();
				st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                st.setQueryTimeout(0);
				while (queryCount.getAndIncrement() < QUERY_SQL_NUM) {
					count++;
					rs = st.executeQuery(sql+"'"+getRecordName()+"'");
					if(rs.next()){
					   rs.getLong("id");
					}
				}
			}finally{
				close(conn);
				close(st);
				close(rs);
			}
	     }
		
		private void queryById() throws Exception {
            //测试长连接(1个线程生命周期只创建一次连接)
		    Connection conn = null;
		    Statement st = null;
		    ResultSet rs = null;
			try{
				conn = getConnection();
				st = conn.createStatement();
                st.setQueryTimeout(0);
				while (queryCount.getAndIncrement() < QUERY_SQL_NUM) {
					count++;
					rs = st.executeQuery(sql+getRecordId());
					if(rs.next()){
					   rs.getLong("id");
					}
				}
			}finally{
				close(conn);
				close(st);
				close(rs);
			}
	     }
		
		private void updateById() throws Exception {
            //测试长连接(1个线程生命周期只创建一次连接)
		    Connection conn = null;
		    Statement st = null;
		    ResultSet rs = null;
			try{
				conn = getConnection();
				st = conn.createStatement();
                st.setQueryTimeout(0);
				while (queryCount.getAndIncrement() < QUERY_SQL_NUM) {
					count++;
					st.executeUpdate(sql+getRecordId());
				}
			}finally{
				close(conn);
				close(st);
				close(rs);
			}
	     }
		
		private void updateByName() throws Exception {
            //测试长连接(1个线程生命周期只创建一次连接)
		    Connection conn = null;
		    Statement st = null;
		    ResultSet rs = null;
			try{
				conn = getConnection();
				st = conn.createStatement();
                st.setQueryTimeout(0);
				while (queryCount.getAndIncrement() < QUERY_SQL_NUM) {
					count++;
					st.executeUpdate(sql+"'"+getRecordName()+"'");
				}
			}finally{
				close(conn);
				close(st);
				close(rs);
			}
	     }


	}
}
